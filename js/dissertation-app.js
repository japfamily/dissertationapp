(function(){
    var app = angular.module('dissertationApp', ['ngStorage']);

    app.controller('PagesController', function($scope, $localStorage) {
        var pagesCtrl = this;

        $scope.$storage = $localStorage.$default({
            completedPages: [
                { count: 2,  date: new Date("November 4, 2015 00:00:01").valueOf() },
                { count: 2,  date: new Date("November 3, 2015 00:00:01").valueOf() },
                { count: 4,  date: new Date("November 2, 2015 00:00:01").valueOf() },
                { count: 40, date: new Date("November 1, 2015 00:00:01").valueOf() }
                /*
                { count: 30, date: new Date().setDate(new Date().getDate()-2) },
                { count: 10, date: Date.now()                                 },
                { count: 50, date: new Date().setDate(new Date().getDate()-4) },
                { count: 20, date: new Date().setDate(new Date().getDate()-1) },
                { count: 40, date: new Date().setDate(new Date().getDate()-3) }
                */
            ]
        });

        pagesCtrl.totalPages = 300;

        pagesCtrl.completedPages = $scope.$storage.completedPages;

        pagesCtrl.getCompletedPageCnt = function() {
            var cnt = 0;
            for (i = 0; i < pagesCtrl.completedPages.length; i++) {
                cnt += pagesCtrl.completedPages[i].count;
            }
            return cnt;
        };

        pagesCtrl.progress = function() {
            var percentComplete = 100 * pagesCtrl.getCompletedPageCnt() / pagesCtrl.totalPages;
            return percentComplete.toFixed(1);
        };

        pagesCtrl.addPages = function() {
            var now = Date.now();
            pagesCtrl.completedPages.unshift({ count: pagesCtrl.newPages, date: now });
            pagesCtrl.newPages = '';
        };

        pagesCtrl.delete = function(count, date) {
            var item = { count, date };
            for (i = 0; i < pagesCtrl.completedPages.length; i++) {
                if (pagesCtrl.completedPages[i].count === count &&
                        pagesCtrl.completedPages[i].date === date) {
                    pagesCtrl.completedPages.splice(i, 1);
                    break;
                }
            }
        };

        pagesCtrl.drawCircularProgressBar = function() {
            var $pc = $('#progressController');
            var $pCaption = $('.progress-bar p');
            var iProgress = document.getElementById('inactiveProgress');
            var aProgress = document.getElementById('activeProgress');
            var iProgressCTX = iProgress.getContext('2d');


            drawInactive(iProgressCTX);

            $pc.on('change', function(){
                var percentage = $(this).val() / 100;
                console.log(percentage + '%');
                drawProgress(aProgress, percentage, $pCaption);
            });

            function drawInactive(iProgressCTX){
                iProgressCTX.lineCap = 'square';

                //outer ring
                iProgressCTX.beginPath();
                iProgressCTX.lineWidth = 15;
                iProgressCTX.strokeStyle = '#e1e1e1';
                iProgressCTX.arc(137.5,137.5,129,0,2*Math.PI);
                iProgressCTX.stroke();

                //progress bar
                iProgressCTX.beginPath();
                iProgressCTX.lineWidth = 0;
                iProgressCTX.fillStyle = '#e6e6e6';
                iProgressCTX.arc(137.5,137.5,121,0,2*Math.PI);
                iProgressCTX.fill();

                //progressbar caption
                iProgressCTX.beginPath();
                iProgressCTX.lineWidth = 0;
                iProgressCTX.fillStyle = '#fff';
                iProgressCTX.arc(137.5,137.5,100,0,2*Math.PI);
                iProgressCTX.fill();

            }
            function drawProgress(bar, percentage, $pCaption){
                var barCTX = bar.getContext("2d");
                var quarterTurn = Math.PI / 2;
                var endingAngle = ((2*percentage) * Math.PI) - quarterTurn;
                var startingAngle = 0 - quarterTurn;

                bar.width = bar.width;
                barCTX.lineCap = 'square';

                barCTX.beginPath();
                barCTX.lineWidth = 20;
                barCTX.strokeStyle = '#76e1e5';
                barCTX.arc(137.5,137.5,111,startingAngle, endingAngle);
                barCTX.stroke();

                $pCaption.text( (parseInt(percentage * 100, 10)) + '%');
            }

            var percentage = $pc.val() / 100;
            drawProgress(aProgress, pagesCtrl.progress()/100, $pCaption);
        };

    });
})();
